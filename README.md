### Envoice 

# MongoIdentity [![Build status](https://ci.appveyor.com/api/projects/status/09c4fnv2ov54vpwm?svg=true)](https://ci.appveyor.com/project/christophla/conditions)

AspNet Identity storage for MongoDB.

## Supported Platforms
* .NET 2.0

## Installation

https://www.myget.org/feed/envoice/package/nuget/Envoice.MongoIdentity

Add dependency to you project.json:

``` bash

dotnet add package Envoice.MongoIdentity --version 1.0.0 --source https://www.myget.org/F/envoice/api/v3/index.json
```
